tylip-s3
========

Collection of pure functional wrappers around
[AWS SDK for Java](https://aws.amazon.com/sdk-for-java/) for S3 based on 
[Cats](https://typelevel.org/cats/)
and [Cats Effect](https://typelevel.org/cats-effect/) equipped with
[Slick](https://scala-slick.org/) and 
[PureConfig](https://pureconfig.github.io/) integration.
