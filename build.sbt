ThisBuild / evictionErrorLevel := Level.Info
ThisBuild / Compile / doc / sources := Nil
ThisBuild / Compile / packageDoc / publishArtifact  := false

val tylipPublic =
  "tylip-public" at "https://tylip.jfrog.io/artifactory/tylip-public/"
val tylipPublicStage =
  "tylip-public-stage" at "https://tylip.jfrog.io/artifactory/tylip-public-stage/"

ThisBuild / publishTo := Some(
  if (sys.env.get("ENVIRONMENT_ID").contains("prod")) tylipPublic
  else tylipPublicStage
)

ThisBuild / credentials += Credentials(Path.userHome / ".sbt" / ".credentials")

val commonSettings = Seq(
  scalaVersion := "2.13.11",
  organization := "com.tylip",
  resolvers += tylipPublic,
  scalafmtOnCompile := !insideCI.value,
  scalacOptions ++= Seq(
    "-deprecation", // Emit warning and location for usages of deprecated APIs.
    "-feature", // Emit warning and location for usages of features that should be imported explicitly.
    "-unchecked", // Enable additional warnings where generated code depends on assumptions.
    "-Ywarn-dead-code", // Warn when dead code is identified.
    "-Ymacro-annotations",
  ),
  scalacOptions ++= (
    if (insideCI.value) Seq("-Xfatal-warnings")
    else                Seq()
  ),
  addCompilerPlugin("com.olegpy" %% "better-monadic-for" % "0.3.1"),
)

lazy val s3Core = project.in(file("modules/s3-core"))
  .settings(commonSettings)
  .settings(
    name := "tylip-s3-core",
    libraryDependencies ++= Seq(
      "com.amazonaws" % "aws-java-sdk-s3" % "1.12.505",
    ),
  )

lazy val s3Cats = project.in(file("modules/s3-cats"))
  .settings(commonSettings)
  .settings(
    name := "tylip-s3-cats",
    libraryDependencies ++= Seq(
      "org.typelevel" %% "cats-core" % "2.9.0",
      "org.typelevel" %% "cats-effect" % "3.5.1",
    ),
  )
  .dependsOn(s3Core)

lazy val s3Slick = project.in(file("modules/s3-slick"))
  .settings(commonSettings)
  .settings(
    name := "tylip-s3-slick",
    libraryDependencies ++= Seq(
      "com.typesafe.slick" %% "slick" % "3.4.1",
    ),
  )
  .dependsOn(s3Core)

lazy val s3PureConfig = project.in(file("modules/s3-pureconfig"))
  .settings(commonSettings)
  .settings(
    name := "tylip-s3-pureconfig",
    libraryDependencies ++= Seq(
      "com.github.pureconfig" %% "pureconfig" % "0.17.4",
    ),
  )
  .dependsOn(s3Core)

lazy val s3Root = (project in file("."))
  .aggregate(s3Core, s3Cats, s3Slick, s3PureConfig)
  .settings(
    name := "tylip-s3-root",
    publish / skip := true,
  )
