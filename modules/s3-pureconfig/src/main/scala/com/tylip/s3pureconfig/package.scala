package com.tylip

import com.tylip.s3core.{S3BucketKey, S3ConnectionConfig, S3ObjectKey}
import pureconfig.generic.ProductHint
import pureconfig.{CamelCase, ConfigFieldMapping, ConfigReader}
import pureconfig.generic.semiauto.deriveReader

package object s3pureconfig {
  implicit def productHint[T]: ProductHint[T] =
    ProductHint[T](fieldMapping = ConfigFieldMapping(CamelCase, CamelCase))
  implicit val s3ObjectKeyReader: ConfigReader[S3ObjectKey] =
    deriveReader
  implicit val s3BucketKeyReader: ConfigReader[S3BucketKey] =
    deriveReader
  implicit val s3ConnectionConfigReader: ConfigReader[S3ConnectionConfig] =
    deriveReader
}
