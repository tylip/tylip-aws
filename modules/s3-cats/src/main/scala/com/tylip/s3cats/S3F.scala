package com.tylip.s3cats

import cats.effect.kernel.{Resource, Sync}
import cats.syntax.applicativeError._
import cats.syntax.apply._
import cats.syntax.flatMap._
import cats.syntax.functor._
import cats.syntax.option._
import cats.syntax.traverse._
import com.amazonaws.HttpMethod
import com.amazonaws.auth._
import com.amazonaws.client.builder.AwsClientBuilder
import com.amazonaws.regions.Regions
import com.amazonaws.services.s3.model._
import com.amazonaws.services.s3.{AmazonS3, AmazonS3ClientBuilder}
import com.tylip.s3core._

import java.io.InputStream
import java.net.URL
import java.time.Instant
import java.util.Date
import scala.jdk.CollectionConverters._

final class S3F[F[_]: Sync](s3: AmazonS3) {

  def getObjectKeys(
    bucketKey: S3BucketKey,
  ): F[Seq[S3ObjectKey]] =
    Sync[F]
      .delay(s3.listObjects(bucketKey.value)).map(
        _.getObjectSummaries.asScala.toSeq.map(_.getKey).map(S3ObjectKey.apply),
      )

  def getObject(
    key: S3ObjectKey,
    bucket: S3BucketKey,
  ): Resource[F, Option[S3Object]] =
    Resource.make(
      Sync[F].delay(s3.getObject(bucket.value, key.value).some).recover {
        case s3Exception: AmazonS3Exception
            if s3Exception.getErrorCode == "NoSuchKey" =>
          None
      },
    )(_.traverse(s3Object => Sync[F].delay(s3Object.close())).as(()))

  def doesObjectExist(
    key: S3ObjectKey,
    bucket: S3BucketKey,
  ): F[Boolean] =
    Sync[F].delay(s3.doesObjectExist(bucket.value, key.value))

  def move(
    sourceBucket: S3BucketKey,
    sourceKey: S3ObjectKey,
    targetBucket: S3BucketKey,
    targetKey: S3ObjectKey,
  ): F[Unit] =
    Sync[F].delay(
      s3.copyObject(
        sourceBucket.value,
        sourceKey.value,
        targetBucket.value,
        targetKey.value,
      ),
    ) *>
      Sync[F].delay(
        s3.deleteObject(
          sourceBucket.value,
          sourceKey.value,
        ),
      )

  def move(
    sourceBucket: S3BucketKey,
    sourceKey: S3ObjectKey,
    targetBucket: S3BucketKey,
  ): F[Unit] =
    move(sourceBucket, sourceKey, targetBucket, sourceKey)

  def putObject(
    key: S3ObjectKey,
    inputStream: InputStream,
    contentLength: Option[Long],
    bucket: S3BucketKey,
  ): F[PutObjectResult] =
    for {
      metadata <- Sync[F].delay {
        val result = new ObjectMetadata()
        contentLength.foreach(result.setContentLength)
        result
      }
      result <- Sync[F].delay(
        s3.putObject(
          bucket.value,
          key.value,
          inputStream,
          metadata,
        ),
      )
    } yield result

  def presignGet(
    bucket: S3BucketKey,
    key: S3ObjectKey,
    expiration: Instant,
    method: HttpMethod,
  ): F[URL] =
    Sync[F]
      .delay(
        s3.generatePresignedUrl(
          bucket.value,
          key.value,
          Date.from(expiration),
          method,
        ),
      )

}

object S3F {

  def apply[F[_]](implicit v: S3F[F]): S3F[F] = v

  def init[F[_]: Sync](connection: S3ConnectionConfig): S3F[F] = {
    implicit val client: AmazonS3 = AmazonS3ClientBuilder
      .standard()
      .withPathStyleAccessEnabled(true)
      .withEndpointConfiguration(
        new AwsClientBuilder.EndpointConfiguration(
          connection.endpoint,
          Regions.US_EAST_1.name(),
        ),
      )
      .withCredentials(
        BasicCredentialsProvider(connection.accessKey, connection.secretKey),
      )
      .build()
    new S3F(client)
  }

}

final case class BasicCredentialsProvider(accessKey: String, secretKey: String)
    extends AWSCredentialsProvider {
  override def getCredentials: AWSCredentials =
    new BasicAWSCredentials(accessKey, secretKey)
  override def refresh(): Unit = ()
}
