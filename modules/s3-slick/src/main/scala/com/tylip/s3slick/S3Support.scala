package com.tylip.s3slick

import com.tylip.s3core.S3ObjectKey
import slick.ast.BaseTypedType

trait S3Support { self: slick.relational.RelationalProfile =>

  trait S3Implicits extends API {

    implicit val s3ObjectKeyMapping: BaseTypedType[S3ObjectKey] =
      self.MappedColumnType.base(_.value, S3ObjectKey.apply)

  }

}
