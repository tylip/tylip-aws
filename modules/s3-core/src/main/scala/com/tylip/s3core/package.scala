package com.tylip

import com.amazonaws.services.s3.model.S3Object

package object s3core {

  implicit final class S3ObjectKeyS3ObjectOps(val s3o: S3Object)
      extends AnyVal {
    def key: S3ObjectKey = S3ObjectKey(s3o.getKey)
  }

}
