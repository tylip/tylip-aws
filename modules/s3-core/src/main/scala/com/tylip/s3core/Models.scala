package com.tylip.s3core

final case class S3BucketKey(value: String) extends AnyVal
final case class S3ObjectKey(value: String) extends AnyVal

final case class S3ConnectionConfig(
  endpoint: String,
  accessKey: String,
  secretKey: String,
)
